/*
 * 
 * jQuery Amortization Schedule Calculator
 * By: Sandro Dzneladze [http://jasc.idev.ge]
 *
 * Copyright 2014 Sandro Dzneladze
 *
 * Modified for mobile interface, and minor corrections
 * By: James MacLachlan [http://zemlyaozer.com]
 *
 *  
 * You may use this project under MIT license.
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *  
 */

(function($) {
    $.fn.jasc = function(options) {
        var settings = $.extend({/* Defaults */},options);
        var datenow = new Date();
        var amount = localStorage.amount != 'undefined' ? parseInt(localStorage.amount) : 250000;
        var interest = localStorage.interest != 'undefined' ? parseFloat(localStorage.interest) : 3.4;
        var duration = localStorage.duration != 'undefined' ? parseInt(localStorage.duration) : 360;
        var grace = localStorage.grace != 'undefined' ? parseInt(localStorage.grace) : 0;
        var month = localStorage.month != 'undefined' ? parseInt(localStorage.month) : datenow.getMonth();
        var monthSelected = ['','','','','','','','','','','',''];
        monthSelected[month] = 'selected';
        var day = localStorage.day != 'undefined' ? parseInt(localStorage.day) : datenow.getDate();
        var year = localStorage.year != 'undefined' ? parseInt(localStorage.year) : datenow.getFullYear();
        var lefthand = localStorage.lefthand != 'undefined' ? (localStorage.lefthand === 'true') : false;
        var usepad = localStorage.usepad != 'undefined' ? (localStorage.usepad === 'true') : true;
        var controlsHtml = '<div class="loan">' +
                               '<div class="input-group">' +
                                    '<span class="input-group-addon">Loan Amount</span>' +
                                    '<input type="text"  class="form-control num-pad" placeholder="e.g. 250000" id="amortization-schedule-amount" value="'+amount+'"/>' +
                                    '<span class="input-group-addon input-group-addon-right">USD</span>' +
                               '</div>' +
                               '<div class="input-group">' +
                                    '<span class="input-group-addon">Interest</span>' +
                                    '<input type="text" class="form-control num-pad" placeholder="e.g. 32" id="amortization-schedule-interest" value="'+interest+'"/>' +
                                    '<span class="input-group-addon input-group-addon-right">%</span>' +
                               '</div>' +
                               '<div class="input-group">' +
                                    '<span class="input-group-addon">Duration</span>' +
                                    '<input type="text" class="form-control num-pad" placeholder="e.g. 48" id="amortization-schedule-duration" value="'+duration+'"/>' +
                                    '<span class="input-group-addon input-group-addon-right">Months</span>' +
                                '</div>' +
                                '<div class="input-group">' +
                                    '<span class="input-group-addon">Grace Period</span>' +
                                    '<input type="text" class="form-control num-pad" placeholder="e.g. 0" id="amortization-schedule-grace" value="'+grace+'"  />' +
                                    '<span class="input-group-addon input-group-addon-right">Months</span>' +
                                '</div>' +
                           '</div>' +
                           '<div class="date">' +
                             '<div class="input-group force-width-max">' +
                               '<span class="input-group-addon">Start Month</span>' +
                               '<select id="amortization-schedule-month" class="form-control">'+
                                 '<option value="0" '+ monthSelected[0] +'>January</option>'+
                                 '<option value="1" '+ monthSelected[1] +'>February</option>'+
                                 '<option value="2" '+ monthSelected[2] +'>March</option>'+
                                 '<option value="3" '+ monthSelected[3] +'>April</option>'+
                                 '<option value="4" '+ monthSelected[4] +'>May</option>'+
                                 '<option value="5" '+ monthSelected[5] +'>June</option>'+
                                 '<option value="6" '+ monthSelected[6] +'>July</option>'+
                                 '<option value="7" '+ monthSelected[7] +'>August</option>'+
                                 '<option value="8" '+ monthSelected[8] +'>September</option>'+
                                 '<option value="9" '+ monthSelected[9] +'>October</option>'+
                                 '<option value="10" '+ monthSelected[10] +'>November</option>'+
                                 '<option value="11" '+ monthSelected[11] +'>December</option>'+
                               '</select>'+
                             '</div>' +
                                 '<div class="input-group">' +
                                    '<span class="input-group-addon">Start Day</span>' +
                                    '<input type="text" class="form-control num-pad" placeholder="e.g. 28" id="amortization-schedule-day" value="'+day+'"  />' +
                                 '</div>' +
                                 '<div class="input-group">' +
                                    '<span class="input-group-addon">Start Year</span>' +
                                    '<input type="text" class="form-control num-pad" placeholder="e.g. 2014" id="amortization-schedule-year" value="'+year+'" />' +
                                 '</div>' +
                           '</div>' +
                           '<button type="button" class="btn btn-success btn-lg" id="amortization-schedule-submit">Calculate</button>';
        $('#'+settings.controlsID).append(controlsHtml);

        var container = this;

        $('#amortization-schedule-submit').on('click', function(){
            var dataHtml = calculate();
            if (dataHtml) {
                // Insert into dom
                $(container).find('table').remove();
                $(container).append(dataHtml.join(''));
            }
            return false;
        });
         
        $('.num-pad').on('click',function(e) { numenter(e); });

        $('#usepad').on('click',function(e) { localStorage.usepad = $("#usepad").prop('checked'); });
        $("#usepad").prop('checked',usepad);

        $('#lefthand').on('click',function(e) { localStorage.lefthand = $("#lefthand").prop('checked'); });
        $("#lefthand").prop('checked',lefthand);
        
        function calculate() {
            flushOld();

            var loanAmount    = parseInt($('#amortization-schedule-amount').val()),
                loanInterest  = parseFloat($('#amortization-schedule-interest').val()), // %
                loanDuration  = parseInt($('#amortization-schedule-duration').val()), // Month
                loanGrace     = parseInt($('#amortization-schedule-grace').val()), // Month
                loanInitMonth = parseInt($('#amortization-schedule-month').val()),
                loanInitDay   = parseInt($('#amortization-schedule-day').val()),
                loanInitYear  = parseInt($('#amortization-schedule-year').val());

            // validate input
            if (isNaN(loanAmount) || isNaN(loanInterest) || isNaN(loanDuration) || isNaN(loanGrace) || isNaN(loanInitMonth) || isNaN(loanInitDay) || isNaN(loanInitYear) || 
                loanAmount < 1 || loanInterest < 0 || loanInterest > 100 || loanDuration < 1 || loanGrace < 0 || loanInitMonth < 0 || loanInitMonth > 12 || loanInitDay < 0 || loanInitDay > 31 || loanInitYear < 1000 || loanInitYear > 9999) {
                if (isNaN(loanAmount) || loanAmount < 1)                                { $('#amortization-schedule-amount').addClass('error'); } else { $('#amortization-schedule-amount').removeClass('error'); }
                if (isNaN(loanInterest) || loanInterest < 0 || loanInterest > 100)      { $('#amortization-schedule-interest').addClass('error'); } else { $('#amortization-schedule-interest').removeClass('error'); }
                if (isNaN(loanDuration) || loanDuration < 1)                            { $('#amortization-schedule-duration').addClass('error'); } else { $('#amortization-schedule-duration').removeClass('error'); }
                if (isNaN(loanGrace) || loanGrace < 0)                                  { $('#amortization-schedule-grace').addClass('error'); } else { $('#amortization-schedule-grace').removeClass('error'); }
                if (isNaN(loanInitMonth) || loanInitMonth < 0 || loanInitMonth > 12)    { $('#amortization-schedule-month').addClass('error'); } else { $('#amortization-schedule-month').removeClass('error'); }
                if (isNaN(loanInitDay) || loanInitDay < 0 || loanInitDay > 31)          { $('#amortization-schedule-day').addClass('error'); } else { $('#amortization-schedule-day').removeClass('error'); }
                if (isNaN(loanInitYear) || loanInitYear < 1000 || loanInitYear > 9999)  { $('#amortization-schedule-year').addClass('error'); } else { $('#amortization-schedule-year').removeClass('error'); }
                return false;
            } else {
                $.each($('#'+settings.controlsID).find('input'),function(key,val){
                    $(val).removeClass('error');
                });
            }
            localStorage.amount = loanAmount.toString();
            localStorage.interest = loanInterest.toString();
            localStorage.duration = loanDuration.toString();
            localStorage.grace = loanGrace.toString();
            localStorage.month = loanInitMonth.toString();
            localStorage.day = loanInitDay.toString();
            localStorage.year = loanInitYear.toString();

            var loanStart = new Date(loanInitYear,loanInitMonth,loanInitDay);
            var loanTable = [];

            // Create array of the schedule
            var i = 0;
            for (var m = loanInitMonth; m < loanDuration + loanInitMonth; m++) {
                // date
                var d = new Date(loanStart.getTime());
                d.setMonth(m);
                // percent
                if (i > loanGrace) {
                    var pmt = PMT((loanInterest/100)/12,loanDuration-loanGrace,loanAmount);
                    var ipmt = IPMT(loanAmount,pmt,(loanInterest/100)/12,i-loanGrace);
                    var p = -ipmt;
                } else {
                    var p = loanAmount * (loanInterest/100) / 12
                }
                // installment
                if (i >= loanGrace) {
                    var inst = -PMT((loanInterest/100)/12,loanDuration-loanGrace,loanAmount);
                } else {
                    var inst = p;
                }
                // base
                var b = inst - p;
                // balance
                if (i == 0) {
                    var c = loanAmount - b;
                } else {
                    var c = loanTable[i-1].balance - b;
                }
                // hash
                loanTable.push({date:d,base:b,percent:p,installment:inst,balance:c});
                //
                i++;
            }
            // Transform into html
            var loanTableTd = [];
            for (var t = 0; t < loanTable.length; t++) {
                loanTableTd.push((t == 0 ? '<table class="table-striped"><thead><tr><th>#</th><th>Date</th><th>PFG</th><th>I</th><th>PYMT</th><th>BCE</th></tr></thead><tbody>' : '') + '<tr><td style="padding: 1px;">'
                  + (t + 1) +'</td><td style="padding: 2px 2px 2px 2px; border-left: 1px solid #ddd;">'
                  + formatDate(loanTable[t].date) +'</td><td style="border-left: 1px solid #ddd;">'
                  + "$" + roundTo2(loanTable[t].base) +'</td><td style="border-left: 1px solid #ddd;">'
                  + "$" + roundTo2(loanTable[t].percent) +'</td><td style="border-left: 1px solid #ddd;">'
                  + "$" + Math.round(loanTable[t].installment) +'</td><td style="border-left: 1px solid #ddd;">'
                  + "$" + roundTo2(loanTable[t].balance) +'</td></tr>' 
                  + (t == loanTable.length - 1 ? '</tbody></table>' : ''));
            }
            return loanTableTd;
        }
        // Helper functions
        function formatDate(date) {
            return String(date).substr(4,12);
        }
        function roundTo2(num) {
            return parseFloat(Math.round(num * 100) / 100).toFixed(2);
        }
        function PMT(rate,nper,pv,fv,type) {
            // PMT excel function taken from https://gist.github.com/pies/4166888
        	if (!fv) fv = 0;
        	if (!type) type = 0;
        	if (rate == 0) return -(pv + fv)/nper;
        	var pvif = Math.pow(1 + rate, nper);
        	var pmt = rate / (pvif - 1) * -(pv * pvif + fv);
        	if (type == 1) {
        		pmt /= (1 + rate);
        	};
        	return pmt;
        }
        function IPMT(pv,pmt,rate,per) {
            // PMT excel function taken from https://gist.github.com/pies/4166888
        	var tmp = Math.pow(1 + rate, per);
        	return 0 - (pv * tmp * rate + pmt * (tmp - 1));
        }
        function flushOld() {
          $("#schedule-container").html('');
        }
        
        var klock = false;
        var intarget = '';

        function inp(e) {
          if(klock) return false;
          klock = true;
          num = e.target.title;
          var cval = $("#"+intarget).val();
          if(!isNaN(num)) {
            $("#" + intarget).val( cval + '' + num );
          } else if(num == 'del') {
            $("#" + intarget).val(cval.substr(0,cval.length-1));
          } else if(num == 'close') {
            $("#pad").remove();
          } else if(num == '.') {
            $("#" + intarget).val( cval + '' + num );
          }
          return true;
        }
        function ino() {
          klock=false;
        }
        function numenter(e) {
          intarget = e.target.id;
          $('#pad').remove();
          $('#schedule-container').html('');
          if(!$("#usepad").is(":checked")) return;
          var padstyle='';
          var keyPad = "<div id='pad' "+padstyle+">"
            + "<div title='1' class='button'>1</div>"
            + "<div title='2' class='button'>2</div>"
            + "<div title='3' class='button'>3</div>"
            + "<div title='4' class='button'>4</div>"
            + "<div title='5' class='button'>5</div>"
            + "<div title='6' class='button'>6</div>"
            + "<div title='7' class='button'>7</div>"
            + "<div title='8' class='button'>8</div>"
            + "<div title='9' class='button'>9</div>"
            + "<div title='.' class='button'>.</div>"
            + "<div title='0' class='button'>0</div>"
            + "<div title='del' class='button'>&#171;</div>"
            + "<div title='close' class='button button-wide'>Close</div>"
            + "</div>";
          $('#'+intarget).parent().after(keyPad);
          $('#'+intarget).blur();
          if($("#lefthand").is(":checked")) $("#pad").css('float','none');
          $('.button').on('touchstart', function(e) { inp(e); });
          $('.button').on('touchend', function(e) { ino(); });
          $('#pad')[0].scrollIntoView(false);
        }

    }
}(jQuery));

